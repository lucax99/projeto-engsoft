import React, { Component } from 'react';
import { Widget, addResponseMessage } from 'react-chat-widget';
import './style.css'
import 'react-chat-widget/lib/styles.css';

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      
    }
  }
  componentDidMount() {
    addResponseMessage("Olá, me chamo Bilu, como posso lhe ajudar? Digite 1 para saber os Integrantes da equipe");
  }
  handleNewUserMessage = (newMessage) => {


    switch (newMessage) {
      case '1':
        addResponseMessage("Lucas Cavalcante")
        addResponseMessage("Rodrigo Carvalho")
        addResponseMessage("Magvinier Jr")
        addResponseMessage("Dinara Araújo")
        addResponseMessage("Digite 2 para acessar o menu de opções.")
        break;
      case '2':
        addResponseMessage("Para saber a profissão de cada um, Digite 3")
        addResponseMessage("Para Finalizar acesso, Digite 5")
        break;
        case '3':
        addResponseMessage( "Lucas: Desenvolvedor Web" )
        addResponseMessage( "Rodrigo: Engenheiro de Testes" )
        addResponseMessage( "Dinara: Engenheria de Testes" )
        addResponseMessage( "Magvinier: Bancário" )
        addResponseMessage("Para Finalizar acesso, Digite 5")
        break;
        case '5':
        alert('Obrigado Pelo Acesso ')
        break;
    }

    console.log(newMessage)

  }
  render() {
    return (
      <div className="App">
        <Widget
          handleNewUserMessage={this.handleNewUserMessage}
          title="Bilu"
          subtitle="Seu Assistente Virtual"
        />
      </div>
    );
  }
}
export default App;